// On my honor:
// //
// // - I have not discussed the C language code in my program with
// // anyone other than my instructor or the teaching assistants
// // assigned to this course.
// //
// // - I have not used C language code obtained from another student,
// // the Internet, or any other unauthorized source, either modified
// // or unmodified.
// //
// // - If any C language code or documentation used in my program
// // was obtained from an authorized source, such as a text book or
// // course notes, that has been clearly noted with a proper citation
// // in the comments of my program.
// //
// // - I have not designed this program in such a way as to defeat or
// // interfere with the normal operation of the Curator System.
// //
// // David Weisiger
// // dbw@vt.edu

#include "PartitionInteger.h"

/**  Computes a new integer from N by separating all the even digits
 *   digits from N and all the odd digits from N. 
 * 
 * For example:
 *  {23410, EVENHIGH} --> 24031
 *  {23410, ODDHIGH}  --> 31240
 *  {2640, ODDHIGH}   --> 2640
 * 
 * Pre:  N is initialized
 *       Ordering is EVENHIGH or ODDHIGH
 * Returns:  integer obtained by reordering the digits of N as described
 *
 * Restrictions:
 *   - uses only its parameters and local automatic variables
 *     (i.e., no global variables)
 *   - does not make any use of character variables or arrays
 *   - does not read input or write output
 */
uint32_t PartitionInteger(uint32_t N, enum Ordering order) {
      
   // Initialize two variables: one for the even numbers, one for the odd numbers 
   uint32_t evenPart = 0, oddPart = 0;
   // Initialize two counters for even and odd digits
   uint32_t evenDigitCounter = 1, oddDigitCounter = 1;

   // This while loop will iterate through N from the right, truncating one digit 
   // at a time. Once there are no digits left to process, the loop ends.
   while (N > 0) {
      // If N is even, take the digit we are processing from the right, and add it
      // to our evenPart variable. Also, iterate the evenDigitCounter.
      if (N % 2 == 0) {
         // The even part takes the new digit on the right and multiplies it by a
         // factor of 10 to give it the zero places to store the next number. For
         // example: 1234 = 1000 + 200 + 30 + 4
         evenPart += (N % 10) * evenDigitCounter;
         evenDigitCounter *= 10;
      } else {
	 // The same logic goes for odd numbers.
	 oddPart += (N % 10) * oddDigitCounter;
         oddDigitCounter *= 10;
      }	    
      // This will truncate the digit we have finished processing.
      N = N / 10;
   }
   
   // If the order is EVENHIGH, take the even part and add enough zeros to it so 
   // there is space for the odd part (using the oddDigitCounter); then add the 
   // odd part. Vice versa if order = ODDHIGH.
   if (order == EVENHIGH) {
      return evenPart * oddDigitCounter + oddPart;
   } else {
      return  oddPart * evenDigitCounter + evenPart;
   }
}
